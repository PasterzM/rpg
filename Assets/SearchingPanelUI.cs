using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System;

[Serializable]

public struct SearchingPanelUI
{
    public Button mExit;
    public Camera mCamera;
    public Camera mMainCamera;

    Ray ray;
    RaycastHit hit;
    public void StartSearch(Interaction interactObj)
    {
        SearchingInteration searchedObj = interactObj.GetComponent<SearchingInteration>();
        //schowaj przyciski interakcji   
        //wlacz kamere interakcji, wy��cz kamer� g��wna
        //ustaw kamere na odpowiedniej pozycji
        //wlacz kursos
        //wlacz przycisk Exit
        //wlacz oswietlenie

        Cursor.visible = true;
        mCamera.gameObject.SetActive(true);
        mCamera.enabled = true;
        if (mMainCamera != null)
        {
            mMainCamera.enabled = false;
        }

        mCamera.transform.position = searchedObj.mCameraLookingPoint.position;
        mCamera.transform.rotation = searchedObj.mCameraLookingPoint.rotation;
        searchedObj.mLight.gameObject.SetActive(true);
        mExit.gameObject.SetActive(true);
    }

    public void StopSearch(Interaction interactObj)
    {

        Cursor.visible = false;
        mCamera.gameObject.SetActive(false);
        mCamera.enabled = false;
        if (mMainCamera != null)
        {
            mMainCamera.enabled = true;
        }
        mExit.gameObject.SetActive(false);
        EventManager.mInstance.ExcecuteEvent(InteractionState.EXIT_SEARCHING, interactObj);
    }

    public void SetInteractObjToButton(Interaction interObj)
    {
        SearchingPanelUI x = this;
        mExit.onClick.RemoveAllListeners();
        mExit.onClick.AddListener(delegate ()
        {
            x.StopSearch(interObj);
        });
    }

}