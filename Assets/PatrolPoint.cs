﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PatrolPoint : MonoBehaviour {
    public Transform mPeople;
    public List<Transform> mNeighbours;

    void Start() {
        enabled = false;
        if (mPeople != null) {
            NavMeshAgent[] agents = mPeople.GetComponentsInChildren<NavMeshAgent>();
            foreach (NavMeshAgent item in agents) {
                EventManager.mInstance.SetNextPatrolPoint(mNeighbours, this.transform, item);
            }

        }
    }

    public void OnTriggerEnter(Collider other) {
        EventManager.mInstance.SetNextPatrolPoint(mNeighbours, this.transform, other.GetComponent<NavMeshAgent>());
    }
}
