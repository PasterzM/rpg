﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[Serializable]
public class PlayerController
{
    public Transform mTransform;
    //public Transform mHead;
    public float mTurnSmoothTime = 0.1f;

    public Transform mCamera;
    CharacterController mController;
    public Animator mAnimator;


    public float gravity = -12;
    float velocityY;
    public float jumpHeight = 1;

    [Range(0, 1)]
    public float airControllerPercent;

    public float turnSmoothTime = .2f;
    public float mTurnSmoothVelocity;

    public float speedSmoothTime = .2f;
    float speedSmoothVelocity;

    //Prędkości
    public float mWalkSpeed = 2f;
    public float mRunSpeed = 6f;
    public float mCrouchSpeed = 1f;
    [SerializeField] float mCurrentSpeed;

    bool mIsRunning;
    bool mIsCrouching;
    bool mIsBussy; //TODO: zamienic na stale wyliczeniowe, rozmawia, gra, zbiera itp.

    public Wait mKeyboardPressedWait;

    float mAxisX;
    public bool isAttack;

    Ray mRay;
    RaycastHit mHit;
    public Camera mSearchingCamera;
    public LayerMask mSearchLayers;
    public void SetUpController()
    {
        mController = mTransform.GetComponent<CharacterController>();
        mKeyboardPressedWait.StartWait();

        EventManager eventMan = EventManager.mInstance;
        eventMan.AddEventMethod(InteractionState.START_LOOTING, SetIsBusy);
        eventMan.AddEventMethod(InteractionState.START_SEARCHING, SetIsBusy);
        eventMan.AddEventMethod(InteractionState.START_CONVERSATION, SetIsBusy);

        eventMan.AddEventMethod(InteractionState.EXIT_LOOT, SetIsFree);
        eventMan.AddEventMethod(InteractionState.EXIT_CONVERSATION, SetIsFree);
        eventMan.AddEventMethod(InteractionState.EXIT_SEARCHING, SetIsFree);

        Cursor.visible = true;

        //mAnimator.gets
    }

    private void SetIsBusy(Interaction loot)
    {
        mIsBussy = true;
        mCurrentSpeed = 0f;
    }

    private void SetIsFree(Interaction loot)
    {
        mIsBussy = false;
    }
    // Update is called once per frame
    public void Update()
    {
        switch (GameVariables.mInstance.mGameState)
        {
            case GAME_STATE.PLAY:
                if (!mIsBussy)
                {
                    if (!mKeyboardPressedWait.IsWaiting())
                    {
                        if (KeyboardPressedOnePerTime())
                            mKeyboardPressedWait.StartWait();
                    }
                    Keyboard();
                    Mouse();
                    if (!isAttack)
                        Move();
                }
                else
                {
                    KeyboardWithoudBussy();
                    MouseWidhoutBussy();
                }
                mKeyboardPressedWait.Update();
                break;
            case GAME_STATE.CONVERSASTION: break;
            case GAME_STATE.CUTSCENE: break;
        }


    }

    private void MouseWidhoutBussy()
    {
        if (Cursor.visible)
        {
            mRay = mSearchingCamera.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(mRay, out mHit, Mathf.Infinity,mSearchLayers))
            {
                if (Input.GetMouseButtonDown(0))
                    Debug.Log(mHit.collider.name);
            }
        }
    }

    private void KeyboardWithoudBussy()
    {
        if (Input.GetKey(KeyCode.Escape))
        {

            EventManager.mInstance.ExcecuteEvent(InteractionState.EXIT_LOOT, null);
            EventManager.mInstance.ExcecuteEvent(InteractionState.EXIT_CONVERSATION, null);//TODO: usun pozniej, poniewaz nie mozna wyjsc z rozmowy
            EventManager.mInstance.ExcecuteEvent(InteractionState.EXIT_SEARCHING, null);
        }
    }

    private void Mouse()
    {
        if (mController.isGrounded)
        {
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                mAnimator.SetFloat("speed", 0f);
                mAnimator.SetTrigger("Punch");
                isAttack = true;
                mCurrentSpeed = 0f;
            }
            else if (isAttack && mAnimator.GetAnimatorTransitionInfo(0).IsName("Punch -> Idle/Running"))
            {
                isAttack = false;
            }
        }
    }

    //ruch gracza( znarmalizowany obrot kamera, czy biega )
    void Move()
    {
        Vector2 input = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        Vector2 inputDir = input.normalized;

        if (inputDir != Vector2.zero)
        {
            float targetRotation = Mathf.Atan2(inputDir.x, inputDir.y) * Mathf.Rad2Deg + mCamera.eulerAngles.y /*+ cameraOffsetY*/;
            mTransform.eulerAngles = Vector3.up * Mathf.SmoothDampAngle(mTransform.eulerAngles.y, targetRotation, ref mTurnSmoothVelocity, GetModifiedSmoothTime(turnSmoothTime));
        }

        //mAxisX += Input.GetAxis("Mouse X");
        //Quaternion rotation = Quaternion.Euler(mTransform.rotation.y, mAxisX, 0);
        //mTransform.rotation = rotation;

        //MarkEnemie();

        //aktualna max. predkosc gracza 
        float targetSpeed;
        if (mIsCrouching)
        {
            targetSpeed = mCrouchSpeed * inputDir.magnitude;
        }
        else
        {
            targetSpeed = ((mIsRunning) ? mRunSpeed : mWalkSpeed) * inputDir.magnitude;
        }

        mCurrentSpeed = Mathf.SmoothDamp(mCurrentSpeed, targetSpeed, ref speedSmoothVelocity, GetModifiedSmoothTime(speedSmoothTime));
        //przyspieszenie grawitacyjne (w skrocie)
        velocityY += Time.deltaTime * gravity;


        Vector3 velocity = mTransform.forward * mCurrentSpeed + Vector3.up * velocityY;

        mController.Move(velocity * Time.deltaTime);

        mCurrentSpeed = new Vector2(mController.velocity.x, mController.velocity.z).magnitude;  //.magnitude dlugosc wektora

        mAnimator.SetFloat("speed", mCurrentSpeed);

        if (mController.isGrounded)
        {
            velocityY = 0;
            //mAnimator.SetBool("isJumping", false);
        }

        //float animationSpeedPercent = ((running) ? 1f : .5f) * inputDir.magnitude;
    }

    void Keyboard()
    {
        if (Input.GetKey(KeyCode.LeftShift))
        {
            mIsRunning = true;
            mAnimator.SetBool("isRunning", mIsRunning);
        }
        else
        {
            mIsRunning = false;
            mAnimator.SetBool("isRunning", mIsRunning);
        }

        if (Input.GetKey(KeyCode.E) && !mIsBussy)
        {
            EventManager.mInstance.ExcecuteInteraction(this);
        }
    }

    private bool KeyboardPressedOnePerTime()
    {
        if (Input.GetKey(KeyCode.C))
        {
            mIsCrouching = !mIsCrouching;
            mAnimator.SetBool("isCrouching", mIsCrouching);
            return true;
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            Jump();
            return true;
        }

        return false;
    }

    // private int CheckAlphanumericKeyboard() {
    //     if (Input.GetKey(KeyCode.Alpha1)) return 1;
    //     else
    //         if (Input.GetKey(KeyCode.Alpha2)) return 2;
    //     else
    //         if (Input.GetKey(KeyCode.Alpha3)) return 3;
    //     else
    //         if (Input.GetKey(KeyCode.Alpha4)) return 4;
    //     else
    //         if (Input.GetKey(KeyCode.Alpha5)) return 5;
    //     else
    //         if (Input.GetKey(KeyCode.Alpha6)) return 6;
    //     else
    //         if (Input.GetKey(KeyCode.Alpha7)) return 7;
    //     else
    //         if (Input.GetKey(KeyCode.Alpha8)) return 8;
    //     else
    //         if (Input.GetKey(KeyCode.Alpha9)) return 9;
    //     return 0;
    // }

    void Jump()
    {
        if (mController.isGrounded)
        {
            float jumpVecolity = Mathf.Sqrt(-2 * gravity * jumpHeight);
            velocityY = jumpVecolity;
            mAnimator.SetTrigger("Jump");
        }
    }

    float GetModifiedSmoothTime(float smoothTime)
    {
        if (mController.isGrounded)
        {
            return smoothTime;
        }

        if (airControllerPercent == 0)
        {
            return float.MaxValue;
        }

        return smoothTime / airControllerPercent;
    }
}
