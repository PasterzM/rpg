﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using UnityEngine;

public class DialogueManager : MonoBehaviour
{
    public static DialogueManager mInstance;
    public string mPlace;


    bool mIsConversationActive;
    bool mIsPlayerChoosing;
    bool mIsCheckedType;

    float mDt;
    ConversationInteraction mPerson;

    DialogueNode mActSentence;
    Dialogue mActDialogue;

    public delegate void ShowConversation(DialogueNode node);
    public ShowConversation mShowSentences;

    void Awake()
    {
        //mDialogue = new Dialogue();
        mInstance = this;
        EventManager.mInstance.AddEventMethod(InteractionState.START_CONVERSATION, LoadDialogue);
        //EventManager.mInstance.mDelegates.Add(InteractionState.START_CONVERSATION, LoadDialogue);
        //EventManager.mInstance.mStartConversation += LoadDialogue;
    }

    void LoadDialogue(Interaction interObj)
    {
        ConversationInteraction person = interObj.GetComponent<ConversationInteraction>();
        mPerson = person;
        mIsConversationActive = true;
        string path = Application.dataPath + "/XMLs/" + mPlace + "/" + person.name + ".xml";
        //Debug.Log("Path to dialogue: " + path);




        //Test(path, person);

        XmlSerializer xmlSerializer = new XmlSerializer(typeof(Conversations));
        StreamReader reader = new StreamReader(path);
        Conversations conversations = (Conversations)xmlSerializer.Deserialize(reader);
        reader.Close();
        mActDialogue = conversations.mDialogue.Find(x => x.mId.Contains(mPerson.mConversationId));
        if (mActDialogue != null)
        {
            mActSentence = mActDialogue.mSentences[0];

            mDt = 1f;
            EventManager.mInstance.mChangeGameState(GAME_STATE.CONVERSASTION);
            mShowSentences(mActSentence);
        }
        else
        {
            Debug.Log("Problem z rozmowa, osoba:" + mPerson.name + ", idRozmowy: " + mPerson.mConversationId);
            EndConversation();
        }
    }


    public void SelectSentence(int id)
    {
        mActSentence = mActDialogue.mSentences.Find(x => x.mId == id);
        if (mActSentence.mType.Equals("tree")) mIsPlayerChoosing = true;
        else mIsPlayerChoosing = false;
        mShowSentences(mActSentence);
    }

    private void Update()
    {
        if (mIsConversationActive)
        {
            if (!mIsPlayerChoosing)
            {//jezeli nie jest wyswietlane drzewo decyzji
                if (mDt <= 0f)
                {
                    mDt = 2f;   //TODO: zmienic na zminna z XML ktora informuje o dlugosci trwania wyswietlanej informacji
                    if (!mIsCheckedType)    //czy typ zdania byl sprawdzany
                        CheckTypeOfSentence();
                    if (mActSentence.mNextId < 0)
                    {
                        EndConversation();
                    }
                    else
                    {
                        GetNextSentence();
                    }
                }
                else
                {
                    mDt -= Time.deltaTime;
                }
            }
        }
    }

    private void GetNextSentence()
    {
        mIsCheckedType = false;
        mActSentence = mActDialogue.mSentences.Find(x => x.mId == mActSentence.mNextId);
        mShowSentences(mActSentence);
        if (mActSentence.mType.Contains("tree"))
            mIsPlayerChoosing = true;
        else
            mIsPlayerChoosing = false;
    }

    private void EndConversation()
    {
        mIsCheckedType = false;
        EventManager.mInstance.ExcecuteEvent(InteractionState.EXIT_CONVERSATION, null);
        //EventManager.mInstance.mDelegates[InteractionState.EXIT_CONVERSATION]?.Invoke(null);
        //EventManager.mInstance.mConversationExit(null);
        EventManager.mInstance.mChangeGameState(GAME_STATE.PLAY);
        mIsPlayerChoosing = false;
        mIsConversationActive = false;
        mPerson = null;
    }

    private void CheckTypeOfSentence()
    {
        string type = mActSentence.mType;
        if (!type.Contains("sentence"))
        {
            if (type.Contains("event"))
            {
                GameEvents events = mPerson.mActions.Find(x => x.mId == mActSentence.mOptionName);
                if (events != null)
                {
                    events.mEvents[UnityEngine.Random.Range(0, events.mEvents.Count)]?.Invoke();
                }

            }
            mIsCheckedType = true;
        }
    }

    private void ChangeDialogue()
    {
        if (mActSentence.mOptionName.Contains("none"))
        {    //wylaczenie mozliwosci rozmowy z osoba
            mPerson.mIsEnableToInteract = false;
        }
        else
        {
            mPerson.mConversationId = mActSentence.mOptionName;
        }
    }

    void Test(string path, ConversationInteraction person)
    {
        XmlSerializer xmlSerializer = new XmlSerializer(typeof(Conversations));
        StreamWriter writer = new StreamWriter(path);
        //
        int x = 0;
        Conversations mDialogues = new Conversations();
        Dialogue dialogue = new Dialogue();
        dialogue.mId = "rozmowa1";
        dialogue.mSentences.Add(new DialogueNode(x++, person.name, x + 1, "sentence", "Czesc"));
        dialogue.mSentences.Add(new DialogueNode(x++, "player", x + 1, "sentence", "Witam"));
        dialogue.mSentences.Add(new DialogueNode(x++, person.name, x + 1, "sentence", "Jak sie nazywasz"));
        dialogue.mSentences[2].tree = new List<DialogueTreeNode>();
        dialogue.mSentences[2].tree.Add(new DialogueTreeNode(3, "Julek"));
        dialogue.mSentences[2].tree.Add(new DialogueTreeNode(4, "Edek"));
        dialogue.mSentences[2].tree.Add(new DialogueTreeNode(5, "Antek"));
        dialogue.mSentences.Add(new DialogueNode(x++, "player", x + 1, "sentence", "Hello"));
        dialogue.mSentences.Add(new DialogueNode(x++, person.name, x + 1, "sentence", "Hello"));
        dialogue.mSentences.Add(new DialogueNode(x++, "player", x + 1, "sentence", "Hello"));
        dialogue.mSentences.Add(new DialogueNode(x++, person.name, x + 1, "sentence", "Hello"));
        //xmlSerializer.Serialize(writer, dialogue);
        mDialogues.mDialogue.Add(dialogue);

        dialogue = new Dialogue();
        dialogue.mId = "rozmowa2";
        dialogue.mSentences.Add(new DialogueNode(x++, person.name, x + 1, "sentence", "Czesc"));
        dialogue.mSentences.Add(new DialogueNode(x++, "player", x + 1, "sentence", "Witam"));
        dialogue.mSentences.Add(new DialogueNode(x++, person.name, x + 1, "sentence", "Jak sie nazywasz"));
        dialogue.mSentences[2].tree = new List<DialogueTreeNode>();
        dialogue.mSentences[2].tree.Add(new DialogueTreeNode(3, "Julek"));
        dialogue.mSentences[2].tree.Add(new DialogueTreeNode(4, "Edek"));
        dialogue.mSentences[2].tree.Add(new DialogueTreeNode(5, "Antek"));
        dialogue.mSentences.Add(new DialogueNode(x++, "player", x + 1, "sentence", "Hello"));
        dialogue.mSentences.Add(new DialogueNode(x++, person.name, x + 1, "sentence", "Hello"));
        dialogue.mSentences.Add(new DialogueNode(x++, "player", x + 1, "sentence", "Hello"));
        dialogue.mSentences.Add(new DialogueNode(x++, person.name, x + 1, "sentence", "Hello"));
        mDialogues.mDialogue.Add(dialogue);

        xmlSerializer.Serialize(writer, mDialogues);
        //xmlSerializer.Serialize(writer, mDialogue);
        writer.Close();

        mDialogues = new Conversations();
        StreamReader reader = new StreamReader(path);
        mDialogues = (Conversations)xmlSerializer.Deserialize(reader);
        foreach (var item in mDialogues.mDialogue)
        {
            Debug.Log(item.mId);
            foreach (var item2 in item.mSentences)
            {
                Debug.Log(item2);
            }
        }
        reader.Close();
    }

    public void MakePlayerSaySomethin(string sentence)
    {
        DialogueNode node = new DialogueNode(0, "player", -1, "sentence", sentence);
        mActSentence = node;
        mShowSentences(mActSentence);
        mIsConversationActive = true;
        mDt = 2f;
    }
}