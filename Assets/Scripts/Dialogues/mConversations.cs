﻿using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using UnityEngine;

public class Conversations {
    public List<Dialogue> mDialogue;

    public Conversations() { mDialogue = new List<Dialogue>(); }
}
public class Dialogue {
    [XmlArrayItem("node")]
    public List<DialogueNode> mSentences;
    [XmlAttribute("id")]
    public string mId;

    public Dialogue() { mSentences = new List<DialogueNode>(); }
}