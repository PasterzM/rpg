﻿using System.Collections.Generic;
using System.Xml.Serialization;

public class DialogueNode {
    [XmlAttribute("id")]
    public int mId;
    [XmlAttribute("owner")]
    public string mPerson;
    [XmlAttribute("next")]
    public int mNextId;
    [XmlAttribute("type")]
    public string mType;
    [XmlAttribute("option")]
    public string mOptionName;
    [XmlText]
    public string mText;

    [XmlArrayItem("node")]
    public List<DialogueTreeNode> tree;

    //public List<string> mTree;

    public DialogueNode() { }
    public DialogueNode(int id, string person, int next, string type, string text) {
        mId = id;
        mPerson = person;
        mNextId = next;
        mType = type;
        mText = text;
    }
}

public class DialogueTreeNode {
    [XmlAttribute("next")]
    public int mNextId;
    [XmlText]
    public string mText;

    public DialogueTreeNode() { }
    public DialogueTreeNode(int id, string text) {
        mNextId = id;
        mText = text;
    }
}