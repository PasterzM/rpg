﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Quest {
    public string mName;
    [TextArea(15, 20)]
    public string mDescription;
    public string mDialogueForNotComplete;
    public bool mQuestCompleted;    //TODO: wydarzenie
    public bool mQuestFailed;       //TODO: wydarzenie
    public QuestReward mQuestReward;
    public QuestReward mQuestFailedReward;
    public int mMaxExp;
    public ConversationInteraction mOwner;

    public List<Goal> mTasks;

    public void CheckIsComplete() {
        foreach (var item in mTasks) {
            if (!item.CheckIsComplete()) {
                Debug.Log("Quest " + mName + " is not completed");
                return;
            }
        }
        mQuestCompleted = true;
        QuestManager.mInstance.mCompleteQuest(this);
        Debug.Log("Quest " + mName + " is complete");
        //TODO: pokaz wykonanie zadania
    }
}