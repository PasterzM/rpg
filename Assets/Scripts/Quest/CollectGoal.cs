﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Goal", menuName = "Quest/CollectGoal", order = 1)]
public class CollectGoal : Goal {
    public Item mPrefab;
    public override void Init(Quest quest) {
        mQuest = quest;
        QuestManager.mInstance.mCheckItem += Check;
    }

    public void Check(Item item, int size) {
        if (!mIsComplete && item == mPrefab) {
            mCurrentAmount += size;
            //QuestManager.mInstance.mCompleteQuest(mQuest);
            CheckIsComplete();
            mQuest.CheckIsComplete();
        }
    }
}