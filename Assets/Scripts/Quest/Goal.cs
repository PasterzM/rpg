﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Goal : ScriptableObject {
    public string mName;
    public int mCurrentAmount;
    public int mRequiredAmount;
    public bool mIsComplete;
    [HideInInspector] public Quest mQuest;

    public virtual void Init(Quest quest) { Debug.Log("Goal::Init();"); }
    public void Complete() { mIsComplete = true; }    //return bool
    public bool CheckIsComplete() { if (mCurrentAmount >= mRequiredAmount) { Complete(); return true; } return false; }
}