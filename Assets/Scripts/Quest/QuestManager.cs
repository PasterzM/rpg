﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestManager : MonoBehaviour {
    public static QuestManager mInstance;

    public delegate void AddQuest(Quest quest);
    public delegate void CompleteQuest(Quest quest);
    public delegate void ChangeQuestState(Quest quest);

    public delegate void CheckItem(Item item, int size);

    public AddQuest mAddQuest;
    public CompleteQuest mCompleteQuest;
    public CheckItem mCheckItem;

    public List<Quest> mPlayerQuests;

    void Awake() {
        mInstance = this;
        enabled = false;

        mAddQuest += AddQuestToPlayer;
    }

    void AddQuestToPlayer(Quest quest) {
        mPlayerQuests.Add(quest);

        //tworzenie kopij taskow
        List<Goal> copies = new List<Goal>();
        foreach (var item in quest.mTasks) {
            Goal copy = Instantiate(item);
            copies.Add(copy);
        }
        quest.mTasks = copies;

        foreach (var item in quest.mTasks) {
            item.Init(quest);
        }
    }

    public void ChangeQuest(Quest quest, Goal goal) {
        if (quest.mTasks.Find(x => x == goal))
            quest.mTasks.Remove(goal);
        else
            quest.mTasks.Add(goal);
        quest.CheckIsComplete();
    }
}