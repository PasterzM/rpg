﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public List<Quest> mQuests; //TODO: usun, bo manager posiada liste
    public Dictionary<Item, int> mItems;

    [SerializeField]
    public PlayerController mPlayerController;
    void Start()
    {
        mQuests = new List<Quest>();
        mItems = new Dictionary<Item, int>();

        mPlayerController.SetUpController();

        EventManager.mInstance.mAddItem += AddItem;
        QuestManager.mInstance.mAddQuest += AddQuest;
    }

    void AddQuest(Quest quest)
    {
        mQuests.Add(quest);
    }

    // Update is called once per frame
    void Update()
    {
        mPlayerController.Update();
    }

    public void AddItem(Item item, int size)
    {
        Debug.Log(item + " " + size);
        if (!mItems.ContainsKey(item))
            mItems.Add(item, size);
        else
            mItems[item] += size;
        QuestManager.mInstance.mCheckItem?.Invoke(item, size);
    }

    //void OnDrawGizmos() {
    //    Gizmos.color = Color.blue;
    //    Gizmos.DrawLine(mController.mHead.transform.position, mController.mHead.position + (mController.mCamera.forward * 10f));
    //
    //}

    //button.onClick.AddListener(delegate () { QuestManager.mInstance.mCheckItem(item.Key); });
}
