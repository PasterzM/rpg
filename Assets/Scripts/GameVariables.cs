﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GAME_STATE {
    PLAY,
    CUTSCENE,
    CONVERSASTION
};

public class GameVariables : MonoBehaviour {
    public static GameVariables mInstance;
    public GAME_STATE mGameState;

    void Start() {
        this.enabled = false;
        EventManager.mInstance.mChangeGameState += ChangeGameStatus;
        mInstance = this;
    }

    void ChangeGameStatus(GAME_STATE newState) {
        mGameState = newState;
    }
}
