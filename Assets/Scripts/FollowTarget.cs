﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class FollowTarget : MonoBehaviour
{
    public Transform mTarget;
    public float mDistance;
    Vector3 mGoalPosition;

    Vector3 mVelocity;

    //[Range(0.01f, 1.0f)]
    public float mMinY;
    public float mMaxY;

    float mAxisX;
    float mAxisY;

    public LayerMask mLayerMask;

    float time;
    public float mDuration = 1f;

    void Start()
    {
        tag = "MainCamera";
        mVelocity = Vector3.zero;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        switch (GameVariables.mInstance.mGameState)
        {
            case GAME_STATE.PLAY:
                FollowThePlayer();
                break;
        }
    }

    private void FollowThePlayer()
    {
        mAxisX += Input.GetAxis("Mouse X");
        mAxisY += Input.GetAxis("Mouse Y");

        mAxisY = Mathf.Clamp(mAxisY, mMinY, mMaxY);

        Vector3 dir = new Vector3(0, 0, -mDistance);
        Quaternion rotation = Quaternion.Euler(mAxisY, mAxisX, 0);
        mGoalPosition = mTarget.position + rotation * dir;
        transform.position = mGoalPosition;
        transform.LookAt(mTarget.position);

        CheckCollision();
    }

    private void CheckCollision()
    {
        Vector3 dist = mTarget.position - transform.position;
        Ray ray = new Ray(transform.position, dist.normalized);
        RaycastHit hit;
        if (Physics.Linecast(transform.position, mTarget.position, out hit/*, mLayerMask*/))
        {
            //if (mLayerMask == (mLayerMask | (1 << hit.collider.gameObject.layer))) {
            //Debug.Log("Udalo sie");
            if (time < mDuration)
            {
                transform.position = Vector3.Lerp(transform.position, hit.point + transform.forward, time / mDuration);
                //transform.position = Vector3.SmoothDamp(transform.position, hit.point, ref mVelocity, 0.3f);
                //Debug.Log(transform.position + " " + hit.point);
                //transform.position = Vector3.MoveTowards(transform.position, hit.point, Time.deltaTime);
                //transform.position = hit.point + transform.forward * 2f;            
                time += Time.deltaTime;
            }
            else
            {
                transform.position = hit.point + transform.forward;
            }
        }
        else
        {
            time = 0f;
        }

        //transform.position = Vector3.Lerp(transform.position, mGoalPosition, Time.deltaTime * 10f);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawLine(mTarget.position, transform.position);
    }
}