﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LootInteraction : Interaction {
    [SerializeField] List<Item> mItems;
    [SerializeField] List<int> values;

    public Dictionary<Item, int> mInventory;

    void Start() {
        enabled = false;
        mTypeOfInteraction = "Loot";
        mInventory = new Dictionary<Item, int>();
        for (int i = 0; i < mItems.Count; ++i) {
            mInventory.Add(mItems[i], values[i]);
        }
    }

    //public List<GameObject> mItems; //TODO: itemy do zebrania

    public override void OnTriggerEnter(Collider other) {
        //Debug.Log("loot Enter");
        if (mIsEnableToInteract)
            EventManager.mInstance.ExcecuteEvent(InteractionState.ENTER_LOOT,this);
            //EventManager.mInstance.mDelegates[InteractionState.ENTER_LOOT]?.Invoke(this);
        //EventManager.mInstance.mLootEnter(this);
    }

    public override void OnTriggerExit(Collider other) {
        //Debug.Log("loot Exit");
        EventManager.mInstance.ExcecuteEvent(InteractionState.EXIT_LOOT, this);
        //EventManager.mInstance.mDelegates[InteractionState.EXIT_LOOT]?.Invoke(this);
        //EventManager.mInstance.mLootExit(this);
    }

    public void RemoveItem(Item item) {
        mInventory.Remove(item);
        Debug.Log("loot size: " + mInventory.Count);
        if (mInventory.Count <= 0) {
            //EventManager.mInstance.mLootExit(this);
            GameObject.Destroy(gameObject);
            //TODO: zniszcz element
        }
    }

    public override void ExcecuteInteraction(PlayerController player) {
        EventManager.mInstance.ExcecuteEvent(InteractionState.START_LOOTING, this);
        //EventManager.mInstance.mDelegates[InteractionState.START_LOOTING]?.Invoke(this);
        //EventManager.mInstance.mStartLooting(this);
    }

    private void OnDestroy() {
        //EventManager.mInstance.mLootExit(this);
        EventManager.mInstance.ExcecuteEvent(InteractionState.EXIT_LOOT, this);
        //EventManager.mInstance.mDelegates[InteractionState.EXIT_LOOT]?.Invoke(this);
    }
}
