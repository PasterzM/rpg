﻿using System.Collections.Generic;
using UnityEngine;

public class ConversationWithQuests : ConversationInteraction {
    [Space(20)]
    [SerializeField] List<Quest> mListsOfQeuests;
    Dictionary<string, Quest> mQuests;


    void Start() {
        mQuests = new Dictionary<string, Quest>();

        foreach (var item in mListsOfQeuests) {
            mQuests[item.mName] = item;
        }

        enabled = false;
        mTypeOfInteraction = "Conversation";
    }

    public void GiveQuestToPlayer(string key) {
        if (mQuests.ContainsKey(key)) {
            Quest quest = mQuests[key];
            mQuests.Remove(key);
            QuestManager.mInstance.mCompleteQuest += QuestComplete;
            QuestManager.mInstance.mAddQuest(quest);


            //LoadNextConversation();
        }
    }

    public void QuestComplete(Quest quest) {
        if (quest.mOwner == this) {
            SetNextConversation(quest.mName + "Complete");
        }
    }
}