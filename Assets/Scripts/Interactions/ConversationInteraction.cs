﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ConversationInteraction : Interaction
{
    public string mConversationId;

    [Space(20)]
    public List<GameEvents> mActions;

    private void Start()
    {
        mTypeOfInteraction = "Conversation";
    }

    public override void OnTriggerEnter(Collider other)
    {
        //Debug.Log("ConversationEnter");
        if (mIsEnableToInteract)
            EventManager.mInstance.ExcecuteEvent(InteractionState.ENTER_CONVERSATION, this);
            //EventManager.mInstance.mDelegates[InteractionState.ENTER_CONVERSATION]?.Invoke(this);
        //EventManager.mInstance.mConversationEnter(this);
    }
    public override void OnTriggerExit(Collider other)
    {
        //Debug.Log("ConversationExit");
        EventManager.mInstance.ExcecuteEvent(InteractionState.EXIT_CONVERSATION, this);
        //EventManager.mInstance.mDelegates[InteractionState.EXIT_CONVERSATION]?.Invoke(this);
        //EventManager.mInstance.mConversationExit(this);
    }

    public override void ExcecuteInteraction(PlayerController player)
    {
        EventManager.mInstance.ExcecuteEvent(InteractionState.START_CONVERSATION, this);
        //EventManager.mInstance.mDelegates[InteractionState.START_CONVERSATION]?.Invoke(this);
        //EventManager.mInstance.mStartConversation(this);
    }

    public void SetNextConversation(string newConversation)
    {
        mConversationId = newConversation;
    }
}


[Serializable]
public class GameEvents
{
    public string mId;
    public List<UnityEvent> mEvents;
}