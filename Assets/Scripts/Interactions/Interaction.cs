﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public abstract class Interaction : MonoBehaviour {
    public bool mIsEnableToInteract;

    public void SetIsEnableToInteract(bool isEnable) { mIsEnableToInteract = isEnable; }

    [HideInInspector] public string mTypeOfInteraction;

    public abstract void OnTriggerEnter(Collider other);

    public abstract void OnTriggerExit(Collider other);

    public abstract void ExcecuteInteraction(PlayerController player);

    
}