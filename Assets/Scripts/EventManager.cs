﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public enum InteractionState
{
    ENTER_LOOT,
    EXIT_LOOT,
    ENTER_CONVERSATION,
    EXIT_CONVERSATION,
    ENTER_SEARCHING,
    EXIT_SEARCHING,
    START_CONVERSATION,
    START_LOOTING,
    REFRESH_INTERACT_OBJECTS,
    ADD_ITEM_TO_INVENTORY,
    START_SEARCHING,
    END_SEARCHING
}

public class EventManager : MonoBehaviour
{
    public static EventManager mInstance;

    Dictionary<InteractionState, Action<Interaction>> mDelegates;

    public delegate void AddItemToInventory(Item item, int value);
    public AddItemToInventory mAddItem;

    //wydarzenia dotyczace gry/rozgrywki
    public delegate void ChangeGameState(GAME_STATE newState);
    //public RefreshInteractionObjects mRefreshInteractions;
    public ChangeGameState mChangeGameState;
    public PlayerController mPlayer;
    Queue<Interaction> mInteractionObjects;
    void Awake()
    {
        mInstance = this;
        enabled = false;
        mInteractionObjects = new Queue<Interaction>();
        mDelegates = new Dictionary<InteractionState, Action<Interaction>>();

        mDelegates.Add(InteractionState.ENTER_LOOT, AddInteractObject);
        mDelegates.Add(InteractionState.ENTER_CONVERSATION, AddInteractObject);
        mDelegates.Add(InteractionState.ENTER_SEARCHING, AddInteractObject);

        mDelegates.Add(InteractionState.EXIT_LOOT, RemoveInteractObject);
        mDelegates.Add(InteractionState.EXIT_CONVERSATION, RemoveInteractObject);
        mDelegates.Add(InteractionState.EXIT_SEARCHING, RemoveInteractObject);
    }

    public void AddEventMethod(InteractionState state, Action<Interaction> method)
    {
        if (!mDelegates.ContainsKey(state))
        {
            mDelegates.Add(state, method);
        }
        else
        {
            mDelegates[state] += method;
        }
    }

    public void RemoveEventMethod(InteractionState state, Action<Interaction> method)
    {
        if (!mDelegates.ContainsKey(state))
        {
            mDelegates[state] -= method;
        }
    }
    public void ExcecuteEvent(InteractionState state, Interaction ineractObj)
    {
        if (mDelegates.ContainsKey(state))
            mDelegates[state]?.Invoke(ineractObj);
        else
            Debug.Log("Problem with Event, there is none method to handle event " + state.ToString());
    }

    void AddInteractObject(Interaction obj) { mInteractionObjects.Enqueue(obj); }
    void RemoveInteractObject(Interaction obj)
    {
        if (mInteractionObjects.Count > 0)
        {
            mInteractionObjects.Dequeue();
            if (mInteractionObjects.Count > 0)
                mDelegates[InteractionState.REFRESH_INTERACT_OBJECTS]?.Invoke(mInteractionObjects.Peek());
        }
    }
    public int GetInteracNumber() { return mInteractionObjects.Count; }

    public void ExcecuteInteraction(PlayerController player)
    {
        if (mInteractionObjects.Count > 0)
        {
            Interaction inter = mInteractionObjects.Peek();
            //Debug.Log(inter.name);
            inter.ExcecuteInteraction(player);
        }
    }

    public void SetNextPatrolPoint(List<Transform> neighbours, Transform point, NavMeshAgent agent)
    {
        Debug.Log("Enter Trigger");
        Vector3 oldPos = agent.destination;
        Vector3 goal;
        do
        {
            goal = neighbours[UnityEngine.Random.Range(0, neighbours.Count - 1)].position;
        } while (goal == oldPos);
        agent.SetDestination(goal);
    }
}