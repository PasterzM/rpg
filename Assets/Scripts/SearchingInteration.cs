using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SearchingInteration : Interaction
{
    public Transform mCameraLookingPoint;
    public Light mLight;
    void Start()
    {
        mTypeOfInteraction = "Take a look";
        enabled = false;
    }

    public override void OnTriggerEnter(Collider other)
    {
        if (mIsEnableToInteract)
            EventManager.mInstance.ExcecuteEvent(InteractionState.ENTER_SEARCHING, this);
    }

    public override void OnTriggerExit(Collider other)
    {
        EventManager.mInstance.ExcecuteEvent(InteractionState.EXIT_SEARCHING, this);
    }

    public override void ExcecuteInteraction(PlayerController player)
    {
        EventManager.mInstance.ExcecuteEvent(InteractionState.START_SEARCHING, this);
    }
}