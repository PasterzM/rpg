﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "Item", menuName = "Items/item", order = 1)]
public class Item : ScriptableObject {
    public string mName;
    public Image mIcon;
}