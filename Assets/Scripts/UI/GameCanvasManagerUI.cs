﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System;

public class GameCanvasManagerUI : MonoBehaviour
{
    public static GameCanvasManagerUI mInstance;

    [SerializeField]
    private InteractViewPanelUI mInteractiewViewPanel;

    [SerializeField]
    private LootInteractiveUI mLootInteractivePanel;

    [SerializeField]
    private ConversationUI mConversationPanel;

    [SerializeField]
    private QuestStatusUI mQuestStatusUI;

    [SerializeField]
    private QuestTrackUI mQuestTrackUI;

    [SerializeField]
    public SearchingPanelUI mSearchingUI;

    Action mFunctions;

    void Start()
    {
        ActivateEvents();
        mInstance = this;
        enabled = false;
    }

    void ActivateEvents()
    {
        EventManager eveMan = EventManager.mInstance;
        DialogueManager dialMAn = DialogueManager.mInstance;
        QuestManager questMan = QuestManager.mInstance;

        //if(eveMan.mDelegates.ContainsKey())
        eveMan.AddEventMethod(InteractionState.ENTER_LOOT, mInteractiewViewPanel.ShowButtonToInteract);
        eveMan.AddEventMethod(InteractionState.ENTER_CONVERSATION, mInteractiewViewPanel.ShowButtonToInteract);
        eveMan.AddEventMethod(InteractionState.ENTER_SEARCHING, mInteractiewViewPanel.ShowButtonToInteract);

        eveMan.AddEventMethod(InteractionState.REFRESH_INTERACT_OBJECTS, mInteractiewViewPanel.ShowButtonToInteract);

        eveMan.AddEventMethod(InteractionState.START_LOOTING, mInteractiewViewPanel.StartInteraction);
        eveMan.AddEventMethod(InteractionState.EXIT_LOOT, mInteractiewViewPanel.HideButtonToInteract);

        eveMan.AddEventMethod(InteractionState.START_CONVERSATION, mInteractiewViewPanel.StartInteraction);
        eveMan.AddEventMethod(InteractionState.EXIT_CONVERSATION, mInteractiewViewPanel.HideButtonToInteract);


        eveMan.AddEventMethod(InteractionState.START_LOOTING, mLootInteractivePanel.ShowLoot);
        eveMan.AddEventMethod(InteractionState.EXIT_LOOT, mLootInteractivePanel.HidePanel);

        eveMan.AddEventMethod(InteractionState.START_SEARCHING, mSearchingUI.StartSearch);
        eveMan.AddEventMethod(InteractionState.START_SEARCHING, mSearchingUI.SetInteractObjToButton);
        eveMan.AddEventMethod(InteractionState.END_SEARCHING, mSearchingUI.StopSearch);

        eveMan.AddEventMethod(InteractionState.EXIT_SEARCHING, mInteractiewViewPanel.HideButtonToInteract);
        eveMan.AddEventMethod(InteractionState.EXIT_CONVERSATION, mConversationPanel.HideDialogue);

        dialMAn.mShowSentences += mConversationPanel.ShowDialogue;

        questMan.mAddQuest += mQuestStatusUI.ShowChangeStatus;
        questMan.mCompleteQuest += mQuestStatusUI.ShowChangeStatus;

        questMan.mAddQuest += mQuestTrackUI.TrackQuest;
        questMan.mCompleteQuest += mQuestTrackUI.UpdateStatus;
    }

    void DisableEveents()
    {
        EventManager eveMan = EventManager.mInstance;
        DialogueManager dialMAn = DialogueManager.mInstance;
        QuestManager questMan = QuestManager.mInstance;

        eveMan.RemoveEventMethod(InteractionState.ENTER_LOOT, mInteractiewViewPanel.ShowButtonToInteract);
        eveMan.RemoveEventMethod(InteractionState.ENTER_CONVERSATION, mInteractiewViewPanel.ShowButtonToInteract);
        eveMan.RemoveEventMethod(InteractionState.REFRESH_INTERACT_OBJECTS, mInteractiewViewPanel.ShowButtonToInteract);
        eveMan.RemoveEventMethod(InteractionState.EXIT_SEARCHING, mInteractiewViewPanel.ShowButtonToInteract);

        eveMan.RemoveEventMethod(InteractionState.EXIT_LOOT, mInteractiewViewPanel.HideButtonToInteract);
        eveMan.RemoveEventMethod(InteractionState.EXIT_CONVERSATION, mInteractiewViewPanel.HideButtonToInteract);
        eveMan.RemoveEventMethod(InteractionState.START_LOOTING, mLootInteractivePanel.ShowLoot);
        eveMan.RemoveEventMethod(InteractionState.EXIT_LOOT, mLootInteractivePanel.HidePanel);
        eveMan.RemoveEventMethod(InteractionState.EXIT_CONVERSATION, mConversationPanel.HideDialogue);

        questMan.mAddQuest -= mQuestStatusUI.ShowChangeStatus;
        questMan.mCompleteQuest -= mQuestStatusUI.ShowChangeStatus;
    }

    public void AddFunctionsToWorkInUpdate(Action fun)
    {
        mFunctions += fun;
        this.enabled = true;
    }

    private void Update()
    {
        mFunctions?.Invoke();
    }

    public void RemoveFunctionFromWorkInUpdate(Action fun)
    {
        mFunctions -= fun;
        if (mFunctions == null)
            this.enabled = false;
    }

    private void OnDestroy()
    {
        DisableEveents();
    }
    private void OnDisable()
    {
        DisableEveents();
    }

    private void OnEnable()
    {
        ActivateEvents();
    }
}
