﻿using System;
using UnityEngine;
using UnityEngine.UI;

[Serializable]
public class QuestStatusUI {
    public RectTransform mPanel;
    public Text mStatus;
    public Text mName;

    public void ShowChangeStatus(Quest quest) {
        LeanTween.alpha(mStatus.rectTransform, 1f, 0f);
        LeanTween.alpha(mStatus.rectTransform, 1f, 0f);
        mStatus.transform.localScale = Vector3.one;

        if (quest.mQuestCompleted) {
            mStatus.text = "Complete";
            HideStatus();
        } else if (quest.mQuestFailed) {
            mStatus.text = "Failed";
            HideStatus();
        } else if (!quest.mQuestFailed && !quest.mQuestCompleted) {
            mStatus.text = "Received";
            HideStatus();
        } else {
            HideStatus();
        }
        mName.text = quest.mName;

        mPanel.gameObject.SetActive(true);
    }

    void HideStatus() {
        mStatus.color = Color.green;
        mName.color = Color.blue;
        LeanTween.alphaText(mName.rectTransform, 0f, 5f);
        LeanTween.alphaText(mStatus.rectTransform, 0f, 5f).setOnComplete(delegate () { mPanel.gameObject.SetActive(false); });
    }
}
