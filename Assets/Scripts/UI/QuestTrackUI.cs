﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[Serializable]
public class QuestTrackUI {
    public int mActTrackers;
    public int mMaxTrackQuests;
    public int mMaxGoalsInTrack;

    public GameObject mPrefabOfQuestTracker;

    public Transform mQuestTrackers;

    public void TrackQuest(Quest quest) {
        if (mMaxTrackQuests > mActTrackers) {
            mQuestTrackers.gameObject.SetActive(true);

            if (GetQuestTrackWithName(quest.mName) == null) {

                Transform goalPanel = mQuestTrackers.GetChild(mActTrackers++);

                goalPanel.GetChild(0).GetComponent<Text>().text = quest.mName;
                goalPanel.gameObject.SetActive(true);
                for (int i = 0; i < quest.mTasks.Count && i < mMaxGoalsInTrack; ++i) {
                    goalPanel.GetChild(i + 1).gameObject.SetActive(true);
                    goalPanel.GetChild(i + 1).Find("Number").GetComponent<Text>().text = quest.mTasks[i].mCurrentAmount + "/" + quest.mTasks[i].mRequiredAmount;
                    goalPanel.GetChild(i + 1).Find("Description").GetComponent<Text>().text = quest.mTasks[i].mName;
                }
                //(Goal task in quest.mTasks) {}
            }
        } else {
            Debug.Log("To much quests to track");
        }
    }

    public void UpdateStatus(Quest quest) {
        Transform questTrack = GetQuestTrackWithName(quest.mName);
        if (questTrack.GetChild(0).GetComponent<Text>().text.Equals(quest.mName)) {
            for (int i = 0; i < quest.mTasks.Count; ++i) {
                if (quest.mTasks[i].mCurrentAmount < quest.mTasks[i].mRequiredAmount)
                    questTrack.GetChild(1).Find("Number").GetComponent<Text>().text = quest.mTasks[i].mCurrentAmount + "/" + quest.mTasks[i].mRequiredAmount;
                else
                    questTrack.GetChild(1).Find("Number").GetComponent<Text>().text = quest.mTasks[i].mRequiredAmount + "/" + quest.mTasks[i].mRequiredAmount;
            }
            return;
        }
    }

    Transform GetQuestTrackWithName(string name) {
        foreach (Transform item in mQuestTrackers) {
            if (item.GetChild(0).GetComponent<Text>().text.Equals(name)) {
                return item;
            }
        }
        return null;
    }
}