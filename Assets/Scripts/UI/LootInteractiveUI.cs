﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[Serializable]
public struct LootInteractiveUI
{
    public GameObject mButtonsPrefab;
    public GameObject mPanel;
    public List<Button> mButtons;
    public void ShowLoot(Interaction inter)
    {
        LootInteraction interact = inter.GetComponent<LootInteraction>();
        Cursor.visible = true;
        LootInteractiveUI reffer = this;
        ClearPanel();
        mPanel.SetActive(true);
        Debug.Log("interact.mInventory.Count " + interact.mInventory.Count + " mPanel: " + mPanel.transform.childCount);
        foreach (var item in interact.mInventory)
        {
            GameObject buttPanel = GameObject.Instantiate(mButtonsPrefab);
            buttPanel.transform.SetParent(mPanel.transform);

            Button button = buttPanel.GetComponentInChildren<Button>();
            Image icon = buttPanel.GetComponentInChildren<Image>();
            Text text = buttPanel.GetComponentInChildren<Text>();

            text.text = item.Key.name + " " + item.Value;


            //usuniecie itemu z wlasciciela, dodanie itemu graczowi,
            button.onClick.AddListener(delegate () { interact.RemoveItem(item.Key); });
            button.onClick.AddListener(delegate () { EventManager.mInstance.mAddItem(item.Key, item.Value); });
            button.onClick.AddListener(delegate () { reffer.ShowLoot(interact); });
        }
    }

    void ClearPanel()
    {
        while (mPanel.transform.childCount > 0)
        {
            Transform child = mPanel.transform.GetChild(0);
            child.SetParent(null);
            GameObject.Destroy(child.gameObject);
        }
    }

    public void HidePanel(Interaction interObj)
    {
        LootInteraction lootInteraction = interObj.GetComponent<LootInteraction>();
        Cursor.visible = false;
        if (mPanel != null)
            mPanel.SetActive(false);
    }
}
