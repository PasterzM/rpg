﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[Serializable]
public class ConversationUI
{
    public const int maxSizeOfTree = 10;
    public GameObject mPanel;

    public Text mSentence;
    public GameObject mTree;

    public Sprite mIcon;
    public Button mSelectOptionsPrefab;

    public void ShowDialogue(DialogueNode node)
    {
        String type = node.mType;
        if (type.Contains("sentence") ||
            type.Contains("event"))
        {
            mSentence.gameObject.SetActive(true);
            ShowSentence(node);
        }
        else if (node.mType.Contains("tree"))
        {
            mSentence.gameObject.SetActive(true);
            mTree.SetActive(true);
            ShowSentence(node);
            ShowTree(node);
        }
        else
        {
            Debug.Log("There is some problem with dialogueNode: " + node.mId + ", next id: " + node.mNextId + ", type: " + node.mType);
        }
    }

    public void HideDialogue(Interaction person)
    {
        //ConversationInteraction
        mPanel.gameObject.SetActive(false);
    }

    void ShowTree(DialogueNode options)
    {
        mPanel.gameObject.SetActive(true);
        mTree.gameObject.SetActive(true);
        //TODO: zrob drzewo decyzyjne
        mSentence.gameObject.SetActive(true);
        Cursor.visible = true;
        ClearTree();
        foreach (var item in options.tree)
        {
            Button butt = GameObject.Instantiate(mSelectOptionsPrefab);
            butt.onClick.AddListener(delegate () { DialogueManager.mInstance.SelectSentence(item.mNextId); });
            butt.GetComponentInChildren<Image>().sprite = mIcon;
            butt.GetComponentInChildren<Text>().text = item.mText;
            butt.transform.SetParent(mTree.transform);
        }
    }

    void ShowSentence(DialogueNode sentence)
    {
        ClearTree();
        Cursor.visible = false;
        mPanel.gameObject.SetActive(true);
        mTree.gameObject.SetActive(false);
        mSentence.gameObject.SetActive(true);
        mSentence.text = sentence.mText;
    }

    void ClearTree()
    {
        while (mTree.transform.childCount > 0)
        {
            Transform child = mTree.transform.GetChild(0);
            child.SetParent(null);
            GameObject.Destroy(child.gameObject);
        }
    }

    //TODO: zamknij po skonczeniu rozmowy
}