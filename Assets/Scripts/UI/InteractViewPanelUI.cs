﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[Serializable]
public struct InteractViewPanelUI {
    public GameObject mPanel;
    public Image mIcon;
    public Text mText;

    public void ShowButtonToInteract(Interaction interactObj) {
        if (mPanel != null) {
            mIcon = null;
            mText.text = interactObj.mTypeOfInteraction;
            mPanel.SetActive(true);
        }
    }

    public void HideButtonToInteract(Interaction interacObj) {
        if (mPanel != null && EventManager.mInstance.GetInteracNumber() <= 0)
            mPanel.SetActive(false);
    }

    public void StartInteraction(Interaction interacObj) {
        if (mPanel != null)
            mPanel.SetActive(false);
    }
}
