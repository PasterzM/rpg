﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Wait {
    public float mTimeToWait;
    float mDt;
    bool mIsWaiting;

    public Wait() {
        mDt = 0f;
        mIsWaiting = false;
    }
    public void Update() {
        if (mIsWaiting) {
            if (mDt <= 0f)
                mIsWaiting = false;
            else
                mDt -= Time.deltaTime;
        }
    }

    public void StartWait() { mDt = mTimeToWait; mIsWaiting = true; }
    public bool IsWaiting() { return mIsWaiting; }
}
