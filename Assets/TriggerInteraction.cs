﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TriggerInteraction : Interaction {
    public override void ExcecuteInteraction(PlayerController player) {
        mEvents?.Invoke();
    }

    public override void OnTriggerEnter(Collider other) {
        if (mIsEnableToInteract) {
            ExcecuteInteraction(other.GetComponent<Player>().mPlayerController);
        }
    }

    public override void OnTriggerExit(Collider other) {
    }

    // Start is called before the first frame update
    void Start() {
        enabled = false;
    }

    public UnityEvent mEvents;
}
